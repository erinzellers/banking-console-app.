﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGBank.Models
{
    public class TransferReceipt
    {
        public int AccountNumber { get; set; }
        public int otherAccountNumber { get; set; }
        public decimal TransferAmount { get; set; }
        public decimal NewBalance { get; set; }
        public decimal OtherNewBalance { get; set; }
    }
}
