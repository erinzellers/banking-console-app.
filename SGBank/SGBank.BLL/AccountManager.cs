﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using SGBank.Data;
using SGBank.Models;

namespace SGBank.BLL
{
    public class AccountManager
    {

        public Response<Account> GetAccount(int accountNumber)
        {
            var repo = new AccountRepository();
            var response = new Response<Account>();
            //var otheraccount = repo.LoadAccount(accountNumber);

            try
            {
                var account = repo.LoadAccount(accountNumber);
                

                if (account == null)
                {
                    response.Success = false;
                    response.Message = "Account was not found!";
                }
                else
                {
                    response.Success = true;
                    response.Data = account;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                response.Success = false;
                response.Message = "There was an error.  Please try again later.";
            }

            return response;
        }

        public Response<DepositReciept> Deposit(Account account, decimal amount)
        {
            
            var response = new Response<DepositReciept>();

            try
            {
                if (amount <= 0)
                {
                    response.Success = false;
                    response.Message = "Must deposit a positive value.";
                }
                else
                {
                    account.Balance += amount;
                    var repo = new AccountRepository();
                    repo.UpdateAccount(account);
                    response.Success = true;

                    response.Data = new DepositReciept();
                    response.Data.AccountNumber = account.AccountNumber;
                    response.Data.DepositAmount = amount;
                    response.Data.NewBalance = account.Balance;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public Response<WithdrawReceipt> Withdraw(Account account, decimal amount)
        {

            var response = new Response<WithdrawReceipt>();

            try
            {
                if (amount <= 0)
                {
                    response.Success = false;
                    response.Message = "Must withdraw a positive value.";
                }
                else if (account.Balance < amount)
                {
                    response.Success = false;
                    response.Message = "You can't withdraw more than the amount in your account.";
                }
                else
                {
                    account.Balance -= amount;
                    var repo = new AccountRepository();
                    repo.UpdateAccount(account);
                    response.Success = true;

                    response.Data = new WithdrawReceipt();
                    response.Data.AccountNumber = account.AccountNumber;
                    response.Data.WithdrawAmount = amount;
                    response.Data.NewBalance = account.Balance;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public Response<TransferReceipt> Transfer(Account otheraccount, decimal amount)
        {

            var newresponse = new Response<TransferReceipt>();

            try
            {
                if (amount <= 0)
                {
                    newresponse.Success = false;
                    newresponse.Message = "Must transfer a positive value.";
                }
                else
                {
                    otheraccount.Balance += amount;
                    var repo = new AccountRepository();
                    repo.UpdateAccount(otheraccount);
                    newresponse.Success = true;

                    newresponse.Data = new TransferReceipt();
                    newresponse.Data.AccountNumber = otheraccount.AccountNumber;
                    newresponse.Data.TransferAmount = amount;
                    newresponse.Data.NewBalance = otheraccount.Balance;
                }
            }
            catch (Exception ex)
            {
                newresponse.Success = false;
                newresponse.Message = ex.Message;
            }

            return newresponse;
        }
    }
}
