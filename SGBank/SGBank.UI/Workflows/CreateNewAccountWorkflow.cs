﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.BLL;
using SGBank.Models;
using SGBank.Data;

namespace SGBank.UI.Workflows
{
    public class CreateAccountWorkFlow
    {
        //private Account newaccount;
        

        public void Execute()
        {
            CreateAccount();
           
        }

        

        private void CreateAccount()
        {
            do
            {
                
                var newaccount = new Account();
                Console.Clear();

                Console.WriteLine("To start a new account please enter you first name: ");
                newaccount.FirstName = Console.ReadLine();

                Console.WriteLine("Now enter your last name: ");
                newaccount.LastName = Console.ReadLine();

                Console.WriteLine("And Finally, Enter the amount you want to deposit: ");
                string initBalance = Console.ReadLine();

                decimal initialBalance;
                bool res = decimal.TryParse(initBalance, out initialBalance);
                if (res == false)
                {
                    Console.WriteLine("Please enter a valid numeric value.");
                }

                newaccount.Balance = initialBalance;
                ConfirmAcctData(newaccount);

                Console.ReadKey();

                
            } while (true);
        }

        private void ConfirmAcctData(Account newaccount)
        {
            do
            {
                Console.WriteLine("Please confirm all account data: ");
                Console.WriteLine("\nFirst Name: {0}", newaccount.FirstName);
                Console.WriteLine("Last Name: {0}", newaccount.LastName);
                Console.WriteLine("Initial Deposit Amount: {0}", newaccount.Balance);
                Console.WriteLine("Type 'Y' to confirm account creation or 'N' to cancel. ");
                string confirm = Console.ReadLine();
                Console.Clear();
                if (confirm.Substring(0, 1).ToUpper() == "N")
                {
                    break;
                }
                newaccount.AccountNumber = (GenerateAccountNumber(confirm) + 1);

                Console.WriteLine("Your new account number is: {0}", newaccount.AccountNumber);
                Console.WriteLine("Your current balance is: {0}", newaccount.Balance);

                Console.WriteLine("\nType 'Q' to return to main menu");
                AddToCollection(newaccount);
                string mainMenu = Console.ReadLine().ToUpper();

                switch (mainMenu)
                {
                    case "Q":
                        var main = new MainMenu();
                        main.Execute();
                        break;
                }

            } while (true);
        }

        public int GenerateAccountNumber(string confirm)
        {
            var repo = new AccountRepository();
            var getAll = repo.GetAllAccounts();
            List<int> accountNums = new List<int>();
            foreach (var a in getAll)
            {
                accountNums.Add(a.AccountNumber);
            }
            return accountNums.Count();
        }

        public void AddToCollection( Account newaccount)
        {
            var repo = new AccountRepository();
            var getaccounts = repo.GetAllAccounts();
            
            getaccounts.Add(new Account
            {
                AccountNumber = newaccount.AccountNumber,
                FirstName = newaccount.FirstName,
                LastName = newaccount.LastName,
                Balance = newaccount.Balance
            });

            repo.OverwriteFile(getaccounts);
        }
    }

}