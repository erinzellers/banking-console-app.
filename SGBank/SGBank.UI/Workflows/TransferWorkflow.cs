﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.BLL;
using SGBank.Models;


namespace SGBank.UI.Workflows
{
    class TransferWorkflow
    {

        public void Execute(Account account)
        {
            decimal amount = GetTransferAmount();

            var manager = new AccountManager();

            var resopnse = manager.Withdraw(account, amount);

            if (resopnse.Success)
            {
                Console.Clear();
                Console.WriteLine("Transfer {0:c} from account {1}. The new balance will be {2}.", resopnse.Data.WithdrawAmount, resopnse.Data.AccountNumber, resopnse.Data.NewBalance);
                Console.WriteLine("Press enter to continue");
                Console.ReadKey();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("An error occurred.  {0}", resopnse.Message);
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
            }

            int otheraccountNumber = GetOtherAccountNumberFromUser();
            var otheraccount = manager.GetAccount(otheraccountNumber);
            var newresopnse = manager.Transfer(otheraccount.Data, amount);

            if (newresopnse.Success)
            {
                Console.Clear();
                Console.WriteLine("{0:c} transfered to account {1}. The new balance is {2}.", newresopnse.Data.TransferAmount, newresopnse.Data.AccountNumber, newresopnse.Data.NewBalance);
                Console.WriteLine("Press enter to continue");
                Console.ReadKey();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("An error occurred.  {0}", resopnse.Message);
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
            }

        }

        public int GetOtherAccountNumberFromUser()
        {
            do
            {
                Console.Clear();
                Console.WriteLine("Enter account number receiving transfer: ");
                string input = Console.ReadLine();
                int otherAccountNumber;

                if (int.TryParse(input, out otherAccountNumber))
                    return otherAccountNumber;

                Console.WriteLine("That was not a valid account number.  Press any key to try again...");
                Console.ReadKey();
            } while (true);
        }

        private decimal GetTransferAmount()
        {
            do
            {
                Console.Write("Enter a transfer amount: ");
                var input = Console.ReadLine();
                decimal amount;

                if (decimal.TryParse(input, out amount))
                    return amount;

                Console.WriteLine("That was not a valid amount. Please try again.");
                Console.ReadKey();
            } while (true);
        }
    }
}
