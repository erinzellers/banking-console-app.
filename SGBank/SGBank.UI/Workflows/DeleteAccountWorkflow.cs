﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.Models;
using SGBank.Data;
using SGBank.BLL;

namespace SGBank.UI.Workflows
{
    class DeleteAccountWorkflow
    {
        private Account deletedAccount;

        public void Execute()
        {
            //var deleteaccount = new Account();
            Console.Write("What account do you want to delete? ");
            string input = Console.ReadLine();
            int accountNumber = int.Parse(input);
            var manager = new AccountManager();
            var response = manager.GetAccount(accountNumber);
            if (response.Success)
            {
                deletedAccount = response.Data;
                UserConfirmation();
            }
            else
            {
                Console.WriteLine
                    ("A problem occurred. Please enter account number you wish to delete.");
                Console.WriteLine(response.Message);
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
            }
        }

        public void UserConfirmation()
        {

            Console.Write("You are about to delete the account for {0} {1}. Are you sure? (Y/N)",
                deletedAccount.FirstName, deletedAccount.LastName);
            string confirm = Console.ReadLine();
            do
            {
                if (confirm.ToUpper() != "Y")
                {
                    Console.WriteLine("Error. Please press (Y)es to delete");
                    Console.ReadLine();
                    Console.Clear();
                    UserConfirmation();
                }
                DeleteFromCollection();
            } while (confirm.ToUpper() != "Y");
        }
        
        public void DeleteFromCollection()
        {
            //string emptyFirstName = deletedAccount.FirstName.Replace(deletedAccount.FirstName,"");
            //string emptyLastName = deletedAccount.LastName.Replace(deletedAccount.LastName, "");
            //int emptyAccountNumber = deletedAccount.AccountNumber - deletedAccount.AccountNumber;
            //deletedAccount.Balance -= deletedAccount.Balance;
            var repo = new AccountRepository();

            var getaccounts = repo.GetAllAccounts();

            getaccounts.RemoveAt(deletedAccount.AccountNumber - 1);

            repo.OverwriteFile(getaccounts);

            Console.WriteLine("Your account has been deleted. We hope to see you again.");
            Console.ReadLine();


        }

    }
}
